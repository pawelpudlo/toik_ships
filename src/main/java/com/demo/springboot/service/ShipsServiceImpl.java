package com.demo.springboot.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ShipsServiceImpl implements ShipsService{

    private ArrayList<String> area;
    private int info;

    public ShipsServiceImpl() {
        this.area = new ArrayList<>();
        this.info=0;
        for(int i=0; i<8;i++){
            if(i==1 || i==5 || i==7){
                this.area.add("S");
            }else{
                this.area.add(" ");
            }
        }
    }

    @Override
    public void shot(int i) {
        if(this.area.get(i).equals("S")){
            this.area.set(i,"X");
            this.info = 0;
        }else if(this.area.get(i).equals(" ")){
            this.area.set(i,"O");
            this.info = 1;
        }else if(i>=8 || this.area.get(i).equals("X") || this.area.get(i).equals("O")){
            this.info = 2;
        }
    }

    public int getInfo() {
        return info;
    }

    @Override
    public String getArea() {
        String fullArea = "";
        for (String s : this.area) {
            fullArea = fullArea + s + ",";
        }
        return fullArea;
    }




}
