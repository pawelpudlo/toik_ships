package com.demo.springboot.rest;

import com.demo.springboot.service.ShipsService;
import com.demo.springboot.service.ShipsServiceImpl;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("api/")

public class DocumentApiController {

    private final ShipsService shipsService;

    public DocumentApiController(ShipsService shipsService) {
            this.shipsService = shipsService;

    }


    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentApiController.class);

    @PostMapping("shot/{data}")
    public ResponseEntity<Void> shot(@PathVariable(value = "data") int data){
        LOGGER.info("Last Version: " + this.shipsService.getArea());
        this.shipsService.shot(data);

        if(this.shipsService.getInfo() == 0){
            LOGGER.info(this.shipsService.getArea());
            return new ResponseEntity<>(HttpStatus.OK);
        }else if(this.shipsService.getInfo() == 1){
            LOGGER.info(this.shipsService.getArea());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }else{
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }



    }
}
